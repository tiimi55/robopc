import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;


import java.awt.GridBagConstraints;

import javax.swing.JLabel;

import java.awt.Insets;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;


public class Kayttoliittyma extends JFrame {

	/**
	 *  Lis�tty default serial
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField_tormaysarvo;
	private int tormaysArvo = 1;
	USByhteys usb = new USByhteys();
	private boolean viivanSeuraus = true;
	private int nopeus = 3;
	public int aanet = 1;
	final boolean PUOLI = true;
	final int NOPEUS = 3;
	final int TORMAYSARVO = 28;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {					
					Kayttoliittyma kayttoliittyma = new Kayttoliittyma();
					kayttoliittyma.setVisible(true);					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Kayttoliittyma() {

		// Ohjauspaneelin asetukset
		setTitle("Ohjauspaneeli");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 382, 291);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 40, 69, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
				0, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, 1.0, 0.0,
				Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		// Otsikkojen asetukset
		JLabel lblRobotti = new JLabel("Robotti 5/5");
		GridBagConstraints gbc_lblRobotti = new GridBagConstraints();
		gbc_lblRobotti.anchor = GridBagConstraints.WEST;
		gbc_lblRobotti.insets = new Insets(0, 0, 5, 5);
		gbc_lblRobotti.gridx = 0;
		gbc_lblRobotti.gridy = 1;
		contentPane.add(lblRobotti, gbc_lblRobotti);

		JLabel lblViivanSeuraus = new JLabel("Viivan seuraus:");
		GridBagConstraints gbc_lblViivanSeuraus = new GridBagConstraints();
		gbc_lblViivanSeuraus.anchor = GridBagConstraints.WEST;
		gbc_lblViivanSeuraus.insets = new Insets(0, 0, 5, 5);
		gbc_lblViivanSeuraus.gridx = 0;
		gbc_lblViivanSeuraus.gridy = 4;
		contentPane.add(lblViivanSeuraus, gbc_lblViivanSeuraus);

		// Jcombobox viivanseuraus asetukset
		final JComboBox<String> comboBox_viivanSeuraus = new JComboBox<String>();
		comboBox_viivanSeuraus
				.setToolTipText("Valitse kumpaa puolta robotti seuraa.");
		comboBox_viivanSeuraus.setMaximumRowCount(2);
		GridBagConstraints gbc_comboBox_viivanSeuraus = new GridBagConstraints();
		gbc_comboBox_viivanSeuraus.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_viivanSeuraus.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_viivanSeuraus.gridx = 0;
		gbc_comboBox_viivanSeuraus.gridy = 5;

		// Lis�tty comboboxiin VASEN JA OIKEA
		comboBox_viivanSeuraus.addItem("Oikea");
		comboBox_viivanSeuraus.addItem("Vasen");
		contentPane.add(comboBox_viivanSeuraus, gbc_comboBox_viivanSeuraus);

		// Viivanseuraus compo boxin kuuntelija VASEN VAI OIKEA
		comboBox_viivanSeuraus.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				if (comboBox_viivanSeuraus.getSelectedItem() == "Vasen") {
					setViivanseuraus(false);

				} else
					setViivanseuraus(true);
			}
		});

		// Moottorinopeus asetukset
		JLabel lblNewLabel = new JLabel("Moottorien nopeus:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 6;
		contentPane.add(lblNewLabel, gbc_lblNewLabel);

		// moottoriennopeus combobox asetukset
		final JComboBox<String> comboBox_moottoriennopeus = new JComboBox<String>();
		GridBagConstraints gbc_comboBox_moottoriennopeus = new GridBagConstraints();
		gbc_comboBox_moottoriennopeus.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_moottoriennopeus.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_moottoriennopeus.gridx = 0;
		gbc_comboBox_moottoriennopeus.gridy = 7;

		// Lis�tty comboboxiin HIDAS, KESKINOPEA, NOPEA
		comboBox_moottoriennopeus.addItem("Nopea");
		comboBox_moottoriennopeus.addItem("Keskinopea");
		comboBox_moottoriennopeus.addItem("Hidas");
		

		// Moottori nopeuksien muuttujien valitseminen
		contentPane.add(comboBox_moottoriennopeus,
				gbc_comboBox_moottoriennopeus);

		// Moottori nopeuksien kuuntelija
		comboBox_moottoriennopeus.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				if (comboBox_moottoriennopeus.getSelectedItem() == "Nopea") {
					setNopeus(3);
				}
				
				if (comboBox_moottoriennopeus.getSelectedItem() == "Keskinopea") {
					setNopeus(2);
				}
				
				if (comboBox_moottoriennopeus.getSelectedItem() == "Hidas") {
					setNopeus(1);
				}
			}
		});

		JLabel lblAsetukset = new JLabel("Asetukset");
		GridBagConstraints gbc_lblAsetukset = new GridBagConstraints();
		gbc_lblAsetukset.insets = new Insets(0, 0, 5, 5);
		gbc_lblAsetukset.gridx = 1;
		gbc_lblAsetukset.gridy = 7;
		contentPane.add(lblAsetukset, gbc_lblAsetukset);

		// T�rm�yksen esto arvo otsikon asetukset
		JLabel lblNewLabel_1 = new JLabel("Havaitsemiset\u00E4isyys");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 8;
		contentPane.add(lblNewLabel_1, gbc_lblNewLabel_1);

		// Hae buttonin kuuntelija
		JButton btnPalautaOletukset = new JButton("Palauta oletukset");
		btnPalautaOletukset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				comboBox_viivanSeuraus.setSelectedItem(PUOLI);
				comboBox_moottoriennopeus.setSelectedItem(NOPEUS);
				textField_tormaysarvo.setText(String.valueOf(TORMAYSARVO));
//				String nopeus_;
//				String puoli_;
//
//				usb.lueAsetukset();
//				setViivanseuraus(usb.getPuoli());
//				if (viivanseuraus == true) {
//					puoli_ = "Oikea";
//				} else
//					puoli_ = "Vasen";
//				comboBox_viivanseuraus.setSelectedItem(puoli_);
//				setNopeus(usb.getNopeus());
//				if (nopeus == 1) {
//					nopeus_ = "Hidas";
//				}
//				if (nopeus == 2) {
//					nopeus_ = "Keskinopea";
//				}
//				else nopeus_ = "Nopea";
//				comboBox_moottoriennopeus.setSelectedItem(nopeus_);
//				setTormaysarvo(usb.getEtaisyys());
//				textField_tormaysarvo.setText(String.valueOf(tormaysarvo));
			}
		});
		GridBagConstraints gbc_btnPalautaOletukset = new GridBagConstraints();
		gbc_btnPalautaOletukset.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnPalautaOletukset.insets = new Insets(0, 0, 5, 5);
		gbc_btnPalautaOletukset.gridx = 1;
		gbc_btnPalautaOletukset.gridy = 8;
		contentPane.add(btnPalautaOletukset, gbc_btnPalautaOletukset);

		// t�rm�yksen esto textfield asetukset
		textField_tormaysarvo = new JTextField();
		GridBagConstraints gbc_textField_tormaysarvo = new GridBagConstraints();
		gbc_textField_tormaysarvo.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_tormaysarvo.insets = new Insets(0, 0, 5, 5);
		gbc_textField_tormaysarvo.gridx = 0;
		gbc_textField_tormaysarvo.gridy = 9;
		contentPane.add(textField_tormaysarvo, gbc_textField_tormaysarvo);
		textField_tormaysarvo.setColumns(10);
		textField_tormaysarvo.setText("0");

		// Data buttonin alustus
		JButton btnTallenna = new JButton("Tallenna");
		GridBagConstraints gbc_btnTallenna = new GridBagConstraints();
		gbc_btnTallenna.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnTallenna.insets = new Insets(0, 0, 5, 5);
		gbc_btnTallenna.gridx = 1;
		gbc_btnTallenna.gridy = 9;
		contentPane.add(btnTallenna, gbc_btnTallenna);

		// Tallennabutton kuuntelija
		btnTallenna.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					tormaysArvo = Integer.parseInt(textField_tormaysarvo
							.getText());
				} catch (Exception e) {
					System.err.println("Nyt niit� numeroita vaan!");
				}
				usb.kirjoitaAsetukset(getViivanseuraus(), getNopeus(),
						getTormaysarvo());
				System.out.println("Viiva on: " + getViivanseuraus());
				System.out.println("Nopeus on: " + getNopeus());
				System.out.println("T�rm�ys on: " + getTormaysarvo());
			}
		});

		// Exit Nappula ja sen kuuntelijan alustus
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		GridBagConstraints gbc_btnExit = new GridBagConstraints();
		gbc_btnExit.insets = new Insets(0, 0, 5, 0);
		gbc_btnExit.gridx = 2;
		gbc_btnExit.gridy = 9;
		contentPane.add(btnExit, gbc_btnExit);

		// viivanseuraus otsikon asetukset
		JLabel lblNewLabel_2 = new JLabel((String) null);
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 10;
		contentPane.add(lblNewLabel_2, gbc_lblNewLabel_2);

	}

	/**
	 * @return the tormaysarvo
	 */
	public int getTormaysarvo() {
		return tormaysArvo;
	}

	/**
	 * @param tormaysarvo
	 *            the tormaysarvo to set
	 */
	public void setTormaysarvo(int tormaysarvo) {
		textField_tormaysarvo.setText(String.valueOf(tormaysarvo));
	}

	/**
	 * @return the nopeus
	 */
	public int getNopeus() {
		return nopeus;
	}

	/**
	 * @param nopeus
	 *            the nopeus to set
	 */
	public void setNopeus(int nopeus) {
		this.nopeus = nopeus;
	}

	/**
	 * @return the viivanseuraus
	 */
	public boolean getViivanseuraus() {
		return viivanSeuraus;
	}

	/**
	 * @param viivanseuraus
	 *            the viivanseuraus to set
	 */
	public void setViivanseuraus(boolean viivanseuraus) {
		this.viivanSeuraus = viivanseuraus;
	}

	/**
	 * @return the aanet
	 */
	public int getAanet() {
		return aanet;
	}

	/**
	 * @param aanet
	 *            the aanet to set
	 */
	public void setAanet(int aanet) {
		this.aanet = aanet;
	}

}
