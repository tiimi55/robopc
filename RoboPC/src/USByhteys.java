
import java.io.DataInputStream;
import java.io.DataOutputStream;

import lejos.pc.comm.NXTConnector;

public class USByhteys {

	// Olioiden alustus
	NXTConnector conn = new NXTConnector();
	
	private boolean puoli;
	private int nopeus;
	private int etaisyys;

	// Ota yhteys metodi
	

	public void lueAsetukset() {
		otaYhteys();		
		DataInputStream inDat = new DataInputStream(conn.getInputStream());
		try {
			setPuoli(inDat.readBoolean());
			setNopeus(inDat.readInt());
			setEtaisyys(inDat.readInt());
			inDat.close();
		} catch (Exception ioe) {
			System.err.println("Virhe luettaessa tietoja.");
		}

//		suljeYhteys();
		System.out.println(" luettu puoli: " + getPuoli() + " luettu nopeus " +getNopeus() + " luettu et�isyys " + getEtaisyys());

	}



	public void kirjoitaAsetukset(boolean puoli, int nopeus, int etaisyys) {
		otaYhteys();
		DataOutputStream outDat = new DataOutputStream(conn.getOutputStream());
		try {
			outDat.writeBoolean(puoli);
			outDat.writeInt(nopeus);
			outDat.writeInt(etaisyys);
			outDat.close();

		} catch (Exception ioe) {
			System.err.println("Virhe kirjoitettaessa tietoja.");
		}
//		suljeYhteys();

	}
	/**
	 * Ottaa USB-yhteyden NXTiin
	 */
	
	public void otaYhteys() {

		try {
			conn.connectTo();
			System.out.println("NXT l�ytyi!");
		} catch (Exception ioe) {
			System.err.println("NXT ei l�ydy USB portista");
			System.exit(1);
		}
	}
	/**
	 * Katkaisee USB-yhteyden
	 */

	public void suljeYhteys() {

		try {
			conn.close();
			System.out.println("Closed connection");
		} catch (Exception ioe) {
			System.err.println("Exception Closing connection");
		}

	}

	int getEtaisyys() {
		return etaisyys;
	}

	void setEtaisyys(int etaisyys) {
		this.etaisyys = etaisyys;
	}

	int getNopeus() {
		return nopeus;
	}

	void setNopeus(int nopeus) {
		this.nopeus = nopeus;
	}

	boolean getPuoli() {
		return puoli;
	}

	void setPuoli(boolean puoli) {
		this.puoli = puoli;
	}
}
